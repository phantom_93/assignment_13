for assignment text: please refer/ look to 
Task13_Part1.pdf 
Task13_Part2.pdf 
Task13_Part3.pdf 
Task13_Part4.pdf
-------------------------------------------------------------------------------

Through microsoft's "Nuget packet manager", the following packages are installed.
Microsoft.EntityFrameworkCore.SqlServer,
Microsoft.EntityFrameworkCore.Tools. 

Establishing the connection with Database 
**************************************************************************************************

In order to setup connection with mysql database, the following changes have done to original project. 

Added ConnectionStrings to appsettings.json after "AllowedHosts": "*",
 "ConnectionStrings": {
    "Default": "Data Source= PC7266\\SQLEXPRESS; Initial Catalog=ApplicationDB; Integrated Security=True;"


After that in Startup.cs, within the ConfigureServices function, added the following
string connString = Configuration.GetConnectionString("Default");
            services.AddDbContext<ApplicationDBContext>(o =>
            o.UseSqlServer(connString));
------------------------------------------------------------------------------------------------------------------------

Scaffolding(Generating Controller APIs)
*************************************************************************************************
// as scaffolding  the following functions are added to ApplicationDBContext.cs first 
public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
            :base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId});
        }
Rightclick on Controllers -> Add -> New Scaffolded Item -> API -> API Controller with action, using Entity Framework -> Add
Model class: choose Actor, Character, Movie and Franchise(MovieCharacterAPI.Contexts) seperately. 
Data context class: ApplicationDBContext(MovieCharacterAPI.Contexts) for each
---------------------------------------------------------------------------------------------------------------------------