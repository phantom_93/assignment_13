The following packages are installed for this assignment

**Mircosoft.EntityFrameworkCore.SqlServer**

**Mircosoft.EntityFrameworkCore.Tools**

**Mircosoft.EntityFrameworkCore.Design**

**AutoMapper.Extensions.Microsoft.DependencyInjection**

**Swashbuckle.AspNetCore**

---------------------------------------------------------------------------------------------------------------------------------



This assignment is about an ASP.NET Core API with endpoints namely actors, movies, characters and franchises. With the help of EF (Entity Framework) Core, this API is created. In order to perform CRUD (Create, Read, Update and Delete) Operations within the API, a Generic Respoitory Design Pattern is applied. Additionally, the domain with DTOs (Data Transfer Objects) used within the controllers in order to decouple the data. Documentation of the API is done with the help of a Swagger file during the start of the application. Testing CRUD operations in the API with the Postman collection is also involved in this program. The assignment_13db is the database script that shows the relationships between the entities and the data. 