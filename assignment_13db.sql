USE [master]
GO
/****** Object:  Database [ApplicationDB]    Script Date: 9/6/2020 1:47:05 PM ******/
CREATE DATABASE [ApplicationDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ApplicationDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ApplicationDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ApplicationDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ApplicationDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ApplicationDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ApplicationDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ApplicationDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ApplicationDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ApplicationDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ApplicationDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ApplicationDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [ApplicationDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ApplicationDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ApplicationDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ApplicationDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ApplicationDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ApplicationDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ApplicationDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ApplicationDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ApplicationDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ApplicationDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ApplicationDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ApplicationDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ApplicationDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ApplicationDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ApplicationDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ApplicationDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ApplicationDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ApplicationDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ApplicationDB] SET  MULTI_USER 
GO
ALTER DATABASE [ApplicationDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ApplicationDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ApplicationDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ApplicationDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ApplicationDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ApplicationDB] SET QUERY_STORE = OFF
GO
USE [ApplicationDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/6/2020 1:47:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actors]    Script Date: 9/6/2020 1:47:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[MiddleName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Gender] [int] NOT NULL,
	[DOB] [datetime2](7) NOT NULL,
	[PlaceOfBirth] [nvarchar](max) NULL,
	[Biography] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Characters]    Script Date: 9/6/2020 1:47:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Characters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Gender] [int] NOT NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_Characters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Franchises]    Script Date: 9/6/2020 1:47:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Franchises](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Franchises] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MovieCharacters]    Script Date: 9/6/2020 1:47:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieCharacters](
	[CharacterId] [int] NOT NULL,
	[MovieId] [int] NOT NULL,
	[ActorId] [int] NOT NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_MovieCharacters] PRIMARY KEY CLUSTERED 
(
	[MovieId] ASC,
	[CharacterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movies]    Script Date: 9/6/2020 1:47:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Genre] [nvarchar](max) NULL,
	[ReleaseYear] [datetime2](7) NOT NULL,
	[Director] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
	[Trailer] [nvarchar](max) NULL,
	[FranchiseId] [int] NULL,
 CONSTRAINT [PK_Movies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200903074935_ApplicationDB', N'3.1.7')
GO
SET IDENTITY_INSERT [dbo].[Actors] ON 

INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (1, N'Christian', N'Charles Philip', N'Bale', 0, CAST(N'1974-01-30T00:00:00.0000000' AS DateTime2), N'Haverfordwest, Pembrokeshire, Wales', N'Bio1', N'https://upload.wikimedia.org/wikipedia/commons/0/0a/Christian_Bale-7837.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (2, N'Henry', N'William Dalgliesh', N'Cavill', 0, CAST(N'1983-05-05T00:00:00.0000000' AS DateTime2), N'William Dalgliesh', N'Bio2', N'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Henry_Cavill_%2848417906016%29_%28cropped%29.jpg/639px-Henry_Cavill_%2848417906016%29_%28cropped%29.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (3, N'Charlie', N'Thomas', N'Cox', 0, CAST(N'1982-12-15T00:00:00.0000000' AS DateTime2), N'London, England', N'Bio3', N'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Charlie_Cox_by_Gage_Skidmore.jpg/330px-Charlie_Cox_by_Gage_Skidmore.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (4, N'Krysten', N'Alyce', N'Ritter', 1, CAST(N'1981-12-16T00:00:00.0000000' AS DateTime2), N'London, England', N'Bio4', N'https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Krysten_Ritter_%2836184656415%29_%28cropped%29.jpg/330px-Krysten_Ritter_%2836184656415%29_%28cropped%29.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (5, N'Joaquin', N'Rafael', N'Phoenix', 0, CAST(N'1974-10-28T00:00:00.0000000' AS DateTime2), N'San Juan, Puerto Rico', N'Bio6', N'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Joaquin_Phoenix_in_2018.jpg/330px-Joaquin_Phoenix_in_2018.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (6, N'Thomas', N'Grant', N'Gustin', 0, CAST(N'1990-01-14T00:00:00.0000000' AS DateTime2), N'Norfolk, Virginia, U.S.', N'Bio6', N'https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Grant_Gustin_SDCC_2017_%28cropped%29.jpg/330px-Grant_Gustin_SDCC_2017_%28cropped%29.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (7, N'Jonathan', N'Edward', N'Bernthal', 0, CAST(N'1976-07-20T00:00:00.0000000' AS DateTime2), N'Washington, D.C., U.S.', N'Bio7', N'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Crop_of_Jon_Bernthal_by_Gage_Skidmore_3.jpg/330px-Crop_of_Jon_Bernthal_by_Gage_Skidmore_3.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (8, N'Scarlett', N'Ingrid', N'Johansson', 1, CAST(N'1984-11-22T00:00:00.0000000' AS DateTime2), N'New York City, U.S.', N'Bio8', N'https://m.media-amazon.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_UY317_CR23,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (9, N'Amber', N'Laura', N'Heard', 1, CAST(N'1986-04-22T00:00:00.0000000' AS DateTime2), N'Austin, Texas, U.S.', N'Bio9', N'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Amber_Heard_%2843723454772%29.jpg/330px-Amber_Heard_%2843723454772%29.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (10, N'Anne', N'Jacqueline', N'Hathaway', 1, CAST(N'1982-11-12T00:00:00.0000000' AS DateTime2), N'New York City, U.S.', N'Bio10', N'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Anne_Hathaway_in_2017.png/330px-Anne_Hathaway_in_2017.png')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (11, N'Matt', NULL, N'Letscher', 0, CAST(N'1970-06-26T00:00:00.0000000' AS DateTime2), N'Grosse Pointe, Michigan, U.S.', N'Bio11', N'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Matt_Letscher.jpg/330px-Matt_Letscher.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (12, N'Ryan', N'Rodney', N'Reynolds', 0, CAST(N'1976-10-23T00:00:00.0000000' AS DateTime2), N'Vancouver, British Columbia, Canada', N'Bio12', N'https://media.vanityfair.com/photos/5600612b3a02e0b16051ef0e/2:3/w_640,h_960,c_limit/ryan-reynolds-best-friend-betrayal-gq-october-cover.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (13, N'Melissa', N'Marie', N'Benoist', 1, CAST(N'1988-10-04T00:00:00.0000000' AS DateTime2), N'Houston, Texas, U.S.', N'Bio12', N'https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Melissa_Benoist_%2848477063086%29_%28cropped%29.jpg/330px-Melissa_Benoist_%2848477063086%29_%28cropped%29.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (14, N'Ruby', N'Rose', N'Langenheim', 1, CAST(N'1986-03-20T00:00:00.0000000' AS DateTime2), N'Houston, Texas, U.S.', N'Bio12', N'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Ruby_Rose_XXX_Return_of_Xander_Cage_premiere.png/330px-Ruby_Rose_XXX_Return_of_Xander_Cage_premiere.png')
SET IDENTITY_INSERT [dbo].[Actors] OFF
GO
SET IDENTITY_INSERT [dbo].[Characters] ON 

INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (1, N'Bruce Wayne', N'Dark Knight', 0, N'https://cdn.vox-cdn.com/thumbor/2hcZHf3IO6IRSi9iyqctVhmBT8g=/1400x1050/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19736955/GettyImages_1202254631.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (2, N'Clark Joseph Kent', N'The Man of Steel', 0, N'https://cdn.theatlantic.com/thumbor/xuePShEYRyEQec_THgWcYFhYLnw=/540x0:2340x1800/500x500/media/img/mt/2016/01/superman/original.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (3, N'Matthew Michael Murdock', N'Man Without Fear', 0, N'https://i.pinimg.com/originals/b8/73/38/b873380b1a3f86de2cd73ebca74a6ec4.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (4, N'Jessica Campbell Jones Cage', N'Knightress', 1, N'https://vignette.wikia.nocookie.net/marvelcinematicuniverse/images/4/4f/Jessica_Jones_Season_2_Promotional.png/revision/latest?cb=20180626040112')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (5, N'Arthur Fleck', N'Red Hood', 0, N'https://www.filmweb.no/incoming/article1407833.ece/REPRESENTATIONS/wide_21_1410/Joaquin%20Phoenix%20i%20Joker')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (6, N'Barry Allen', N'Flash', 0, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNi60tY3WdHDomVGVglSrJYFJR5uArkuAxrA&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (7, N'Frank Castle', N'The Punisher', 0, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNi60tY3WdHDomVGVglSrJYFJR5uArkuAxrA&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (8, N'Natasha Romanova', N'Black Widow', 1, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7e0Zr64LZV6ZVHAR_qBBUOd-Y0wr2Yr98Jw&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (9, N'Princess of Xebel', N'Queen Mera', 1, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSOJbGQOUUls7xBy_Ol8NqS14iUq94Q-cvRbw&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (10, N'Selina Kyle', N'The Cat', 1, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTNwZSWAuL12ZIzggVy2YUAuUuuxAogiJs89g&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (11, N'Eobard Thawne', N'Professor Zoom', 1, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2nxAhtictdvN6sS1lSP9OCsoghtlZ3ba9XQ&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (12, N'Wade Winston Wilson', N'Deadpool', 0, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRegQXg4p5cK1YVCELa5WuV6lsqJRQdJuZ_jw&usqp=CAU')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (13, N'Kara Zor-El', N'Supergirl', 1, N'https://www.thewrap.com/wp-content/uploads/2018/09/Supergirl-1.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (14, N'Kate-Kane', N'Batwoman', 1, N'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQkh6aKlkgv2qtNFg8YV0WxAmFx2hj4dqmVsA&usqp=CAU')
SET IDENTITY_INSERT [dbo].[Characters] OFF
GO
SET IDENTITY_INSERT [dbo].[Franchises] ON 

INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (1, N'DC Universe', N'The best cinematic Universe')
INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (2, N'Marvel Cinematic Universe', N'The second to None Universe')
SET IDENTITY_INSERT [dbo].[Franchises] OFF
GO
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (1, 1, 1, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (10, 1, 10, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (2, 2, 2, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (3, 3, 3, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (4, 4, 4, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (5, 5, 5, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (6, 6, 6, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (11, 6, 11, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (7, 7, 7, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (8, 8, 8, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (9, 9, 9, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (12, 10, 12, NULL)
INSERT [dbo].[MovieCharacters] ([CharacterId], [MovieId], [ActorId], [Picture]) VALUES (14, 12, 14, NULL)
GO
SET IDENTITY_INSERT [dbo].[Movies] ON 

INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (1, N'The Dark Knight', N'Action, Crime, Drama', CAST(N'2008-07-25T00:00:00.0000000' AS DateTime2), N'Christopher Nolan', N'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (2, N'Man of Steel', N'Action, Adventure, Sci-Fi', CAST(N'2013-06-21T00:00:00.0000000' AS DateTime2), N'Zack Snyder', N'https://m.media-amazon.com/images/M/MV5BMTk5ODk1NDkxMF5BMl5BanBnXkFtZTcwNTA5OTY0OQ@@._V1_SY1000_CR0,0,676,1000_AL_.jpg', N'https://www.imdb.com/video/vi705668633?playlistId=tt0770828&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (3, N'Daredevil', N'Action, Crime, Drama', CAST(N'2015-04-10T00:00:00.0000000' AS DateTime2), N'Drew Goddard', N'https://m.media-amazon.com/images/M/MV5BODcwOTg2MDE3NF5BMl5BanBnXkFtZTgwNTUyNTY1NjM@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi3349527065?playlistId=tt3322312&ref_=tt_ov_vi', 2)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (4, N'Jessica Jones', N'Action, Crime, Drama', CAST(N'2015-11-20T00:00:00.0000000' AS DateTime2), N'Melissa Rosenberg', N'https://m.media-amazon.com/images/M/MV5BM2QyNmZkNTYtZWQyZi00NDhhLWEzMDItYmIzY2U4ZWVmOWNhXkEyXkFqcGdeQXVyNDg4NjY5OTQ@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.youtube.com/watch?v=nWHUjuJ8zxE', 2)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (5, N'Joker', N'Crime, Drama, Thriller', CAST(N'2019-10-04T00:00:00.0000000' AS DateTime2), N'Todd Phillips', N'https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi1723318041?playlistId=tt7286456&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (6, N'The Flash', N'Action, Adventure, Drama', CAST(N'2014-10-07T00:00:00.0000000' AS DateTime2), N'Glen Winter', N'https://m.media-amazon.com/images/M/MV5BOGU2N2M1YzgtNmE1YS00OGFhLTllMzUtYWU1MDA3NWVmYWYwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi2621750809?playlistId=tt3107288&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (7, N'The Punisher', N'Action, Crime, Drama', CAST(N'2017-11-17T00:00:00.0000000' AS DateTime2), N'Steve Lightfoot', N'https://m.media-amazon.com/images/M/MV5BMTExODIwOTUxNzFeQTJeQWpwZ15BbWU4MDE5MDA0MTcz._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi1024571929?playlistId=tt5675620&ref_=tt_ov_vi', 2)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (8, N'Black Widow', N'Action, Adventure, Sci-Fi', CAST(N'2020-11-06T00:00:00.0000000' AS DateTime2), N'Cate Shortland', N'https://m.media-amazon.com/images/M/MV5BMzFiODE0ZDUtN2IxNC00OTI5LTg4OWItZTE2MjU4ZTk2NjM5XkEyXkFqcGdeQXVyNDYzODU1ODM@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi1202175513?playlistId=tt3480822&ref_=tt_ov_vi', 2)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (9, N'Aquaman', N'Action, Adventure, Fantasy', CAST(N'2018-12-25T00:00:00.0000000' AS DateTime2), N'James Wan', N'https://m.media-amazon.com/images/M/MV5BOTk5ODg0OTU5M15BMl5BanBnXkFtZTgwMDQ3MDY3NjM@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi3588536857?playlistId=tt1477834&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (10, N'Deadpool', N'Action, Adventure, Comedy', CAST(N'2016-02-12T00:00:00.0000000' AS DateTime2), N'Tim Miller', N'https://m.media-amazon.com/images/M/MV5BYzE5MjY1ZDgtMTkyNC00MTMyLThhMjAtZGI5OTE1NzFlZGJjXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi567457049?playlistId=tt1431045&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (11, N'Supergirl', N'Action, Adventure, Drama', CAST(N'2015-07-08T00:00:00.0000000' AS DateTime2), N'Ali Adler, Greg Berlanti, Andrew Kreisberg', N'https://m.media-amazon.com/images/M/MV5BYmZmOTM3NjctZDg3Zi00NTYzLWIzY2UtYWEyOWM2NThhYzg5XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi1617148953?playlistId=tt4016454&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (12, N'Batwoman', N'Action, Adventure, Crime', CAST(N'2019-10-06T00:00:00.0000000' AS DateTime2), N'Caroline Dries', N'https://m.media-amazon.com/images/M/MV5BNWE4MGVhZjUtMjEwNy00NzJlLWE3ZmEtNWMyYmFlZmUzZTA0XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg', N'https://www.imdb.com/video/vi1195032345?playlistId=tt8712204&ref_=tt_ov_vi', 1)
SET IDENTITY_INSERT [dbo].[Movies] OFF
GO
/****** Object:  Index [IX_MovieCharacters_ActorId]    Script Date: 9/6/2020 1:47:05 PM ******/
CREATE NONCLUSTERED INDEX [IX_MovieCharacters_ActorId] ON [dbo].[MovieCharacters]
(
	[ActorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MovieCharacters_CharacterId]    Script Date: 9/6/2020 1:47:05 PM ******/
CREATE NONCLUSTERED INDEX [IX_MovieCharacters_CharacterId] ON [dbo].[MovieCharacters]
(
	[CharacterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Movies_FranchiseId]    Script Date: 9/6/2020 1:47:05 PM ******/
CREATE NONCLUSTERED INDEX [IX_Movies_FranchiseId] ON [dbo].[Movies]
(
	[FranchiseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MovieCharacters]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacters_Actors_ActorId] FOREIGN KEY([ActorId])
REFERENCES [dbo].[Actors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacters] CHECK CONSTRAINT [FK_MovieCharacters_Actors_ActorId]
GO
ALTER TABLE [dbo].[MovieCharacters]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacters_Characters_CharacterId] FOREIGN KEY([CharacterId])
REFERENCES [dbo].[Characters] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacters] CHECK CONSTRAINT [FK_MovieCharacters_Characters_CharacterId]
GO
ALTER TABLE [dbo].[MovieCharacters]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacters_Movies_MovieId] FOREIGN KEY([MovieId])
REFERENCES [dbo].[Movies] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacters] CHECK CONSTRAINT [FK_MovieCharacters_Movies_MovieId]
GO
ALTER TABLE [dbo].[Movies]  WITH CHECK ADD  CONSTRAINT [FK_Movies_Franchises_FranchiseId] FOREIGN KEY([FranchiseId])
REFERENCES [dbo].[Franchises] ([Id])
GO
ALTER TABLE [dbo].[Movies] CHECK CONSTRAINT [FK_Movies_Franchises_FranchiseId]
GO
USE [master]
GO
ALTER DATABASE [ApplicationDB] SET  READ_WRITE 
GO
