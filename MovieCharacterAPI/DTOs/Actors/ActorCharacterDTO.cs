﻿using MovieCharacterAPI.DTOs.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Actors
{
    public class ActorCharacterDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }


        public string LastName { get; set; }

        public IEnumerable<CharacterShortDTO> Characters { get; set; }
    }
}
