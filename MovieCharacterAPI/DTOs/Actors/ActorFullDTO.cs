﻿using MovieCharacterAPI.DTOs.Characters;
using MovieCharacterAPI.DTOs.Franchises;
using MovieCharacterAPI.DTOs.Movies;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Actors
{
    public class ActorFullDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }


        public string LastName { get; set; }

        public Gender Gender { get; set; }
        public DateTime DOB { get; set; } // Date of birth

        public string PlaceOfBirth { get; set; }

        public string Biography { get; set; }

        public string Picture { get; set; }

        public IEnumerable<CharacterDTO> Characters { get; set; }

        public IEnumerable<MovieCharacterDTO> Movies { get; set; }

        //public IEnumerable<FranchiseDTO> Franchises { get; set; }
    }
}
