﻿using MovieCharacterAPI.DTOs.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Movies
{
    public class MovieCharacterDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        public IEnumerable<CharacterDTO> Characters { get; set; }

    }
}
