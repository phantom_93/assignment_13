﻿using MovieCharacterAPI.DTOs.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Franchises
{
    public class FranchiseMoviesDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<MovieDTO> Movies { get; set; }
    }
}
