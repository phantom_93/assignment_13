﻿using MovieCharacterAPI.Controllers;
using MovieCharacterAPI.DTOs.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Franchises
{
    public class FranchiseCharactersDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<CharacterShortDTO> Characters{get; set;} 

        
    }
}
