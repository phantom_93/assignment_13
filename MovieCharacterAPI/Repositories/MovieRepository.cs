﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Controllers;
using MovieCharacterAPI.Migrations;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;


namespace MovieCharacterAPI.Repositories
{
    public class MovieRepository: Repository<ApplicationDBContext, Movie>
    {
        public MovieRepository(DbContextOptions options)
            : base(options)
        {
        }
        public IQueryable<Movie> Queryable
        {
            get { return _Context.Movies.AsQueryable(); }
        }


        #region Add Method
        public override async Task<Movie> Add(Movie movie)
        {
            try
            {
                // add a movie to the database 
                await _Context.Movies.AddAsync(movie);

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return movie;
        }
        #endregion


        #region GetAll Method 
        public override async Task<IEnumerable<Movie>> GetAll()
        {
            IEnumerable<Movie> movies= null;
            try
            {
                // get the whole list of movies
                movies = await _Context.Movies.ToListAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return movies;
        }
        #endregion



        #region GetById Method 
        public override async Task<Movie> GetById(int movieId)
        {
            Movie movie = null;
            try
            {
                // find a movie by its id 
                movie = await _Context.Movies.Where(a => a.Id == movieId).SingleOrDefaultAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return movie;
        }
        #endregion


        #region Update Method
        public override async void Update(params Movie[] movies)
        {
            try
            {
                foreach (Movie movie in movies)
                {
                    // update a movie
                    _Context.Entry(movie).State = EntityState.Modified;
                }

                //await Save();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region Delete Method 
        public override void Delete(int id)
        {

            try
            {
                // delete a movie 
                Movie movie = _Context.Movies.Find(id);
                _Context.Movies.Remove(movie);
            }

            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion




        #region Save Method

        public async override Task<int> Save()
        {
            // save changes 
            return await _Context.SaveChangesAsync();
        }
        #endregion





        public bool Exists(int id)
        {
            return _Context.Movies.Any(m => m.Id == id);
        }

    }
}
