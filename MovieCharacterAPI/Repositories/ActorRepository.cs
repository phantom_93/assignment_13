﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    public class ActorRepository : Repository<ApplicationDBContext, Actor>
    {
        public ActorRepository(DbContextOptions options)
            :base(options)
        {
        }
        public IQueryable<Actor> Queryable
        {
            get { return _Context.Actors.AsQueryable(); }
        }

        /*CRUD = Create, Read, Update and Delete*/
        #region Add Method
        /*Add or Create Actor */
        public override async Task<Actor> Add(Actor actor)
        {
            try
            {
                // add new actor to the database 
                await _Context.Actors.AddAsync(actor);
                //await Save(); 
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return actor;
        }
        #endregion


        #region GetAll Method 
        /*retrieve all actors*/
        public override async Task<IEnumerable<Actor>> GetAll()
        {
            IEnumerable<Actor> actors = null;
            try
            {
                // retrieve the whole list of movies
                actors = await _Context.Actors.ToListAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return actors;
        }
        #endregion



        #region GetById Method 
        /*retrieve actor by his/her id */
        public override async Task<Actor> GetById(int actorId)
        {
            Actor actor  = null;
            try
            {
                // find the actor's id in the database 
                actor = await _Context.Actors.Where(a => a.Id == actorId).SingleOrDefaultAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return actor;
        }
        #endregion


        #region Update Method
        public override async void Update(params Actor[] actors)
        {
            try
            {
                foreach (Actor actor in actors)
                {
                    // update the state of the actor in the database 
                    _Context.Entry(actor).State = EntityState.Modified;
                }

                //await Save();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region Delete Method 
        public override async void Delete(int id)
        {
            
            try
            {
                // delete the actor by his/her id 
                Actor actor = _Context.Actors.Find(id);
                _Context.Actors.Remove(actor);
                await Save(); 
            }

            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion




        #region Save Method

        public async override Task<int> Save()
        {
            // save the changes to the database 
            return await _Context.SaveChangesAsync();
        }
        #endregion




        #region Exit Method
        public bool Exists(int id)
        {
            return _Context.Actors.Any(a => a.Id == id);
        }

        #endregion
    }
}
