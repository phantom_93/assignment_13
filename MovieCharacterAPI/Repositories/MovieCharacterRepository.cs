﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    public class MovieCharacterRepository : Repository<ApplicationDBContext, MovieCharacter>
    {
        DbContextOptions<ApplicationDBContext> options;

        public MovieCharacterRepository(DbContextOptions options): base(options)
        {
            
        }

        public IQueryable<MovieCharacter> Queryable
        {
            get { return _Context.MovieCharacters.AsQueryable(); }
        }

        #region Add Method 
        public override async Task<MovieCharacter> Add (MovieCharacter movieCharacter)
        {
            try { 
               await _Context.MovieCharacters.AddAsync(movieCharacter);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return movieCharacter;
        }
        #endregion



        #region GetAll Method 
        public override async Task<IEnumerable<MovieCharacter>> GetAll()
        {
            IEnumerable<MovieCharacter> movieCharacters = null;
            try
            {
                // retrieve the whole list of moviecharacters 
                movieCharacters = await _Context.MovieCharacters.ToListAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return movieCharacters; 
        }
        #endregion


        #region GetById Method 
        public override Task<MovieCharacter> GetById(int id)
        {
            throw new NotImplementedException();
        }
        public async Task<MovieCharacter> GetByNavigation(int movieId, int characterId)
        {
            MovieCharacter movieCharacter = null; 
            try
            {
                // get a movie character by its movie id and character id 
                movieCharacter = await _Context.MovieCharacters.Where(mc => mc.MovieId.Equals(movieId) && mc.CharacterId.Equals(characterId)).SingleOrDefaultAsync();
                
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return movieCharacter; 
        }

        #endregion


        #region Update Method
        public override async void Update(params MovieCharacter[] movieCharacters)
        {
            try
            {
                foreach (MovieCharacter movieCharacter in movieCharacters)
                {
                    // update a movieCharacter 
                    _Context.Entry(movieCharacter).State = EntityState.Modified;
                }

                //await Save();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region Delete Method 
        public override void Delete(int id)
        {

            try
            {
                // Delete a MovieCharacter by his/ her id 
                MovieCharacter movieCharacter = _Context.MovieCharacters.Find(id);
                _Context.MovieCharacters.Remove(movieCharacter);
            }

            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion




        #region Save Method

        public async override Task<int> Save()
        {
            // save changes 
            return await _Context.SaveChangesAsync();
        }
        #endregion


        public bool Exists(int movieId, int characterId)
        {
            return _Context.MovieCharacters.Any(n => n.MovieId == movieId && n.CharacterId == characterId);
        }


    }
}
