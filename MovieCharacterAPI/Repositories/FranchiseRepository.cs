﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    public class FranchiseRepository: Repository<ApplicationDBContext, Franchise>
    {
        public FranchiseRepository(DbContextOptions options) : base(options) { }
        public IQueryable<Franchise> Queryable
        {
            get { return _Context.Franchises.AsQueryable(); }
        }


        #region Add Method
        public override async Task<Franchise> Add(Franchise franchise)
        {
            try
            {
                // add a franchise to the database 
                await _Context.Franchises.AddAsync(franchise); 
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return franchise; 
        }
        #endregion


        #region GetAll Method 
       
        public override async Task<IEnumerable<Franchise>> GetAll()
        {
            IEnumerable<Franchise> franchises = null;
            try
            {
                // retrieve the whole list of franchises 
                franchises = await _Context.Franchises.ToListAsync(); 
            }
            catch(DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return franchises; 
        }

        #endregion


        #region GetById Method
        public override async Task<Franchise> GetById(int franchiseId)
        {
            Franchise franchise = null;
            try
            {
                // retrieve a franchise from the database by its id 
                franchise = await _Context.Franchises.Where(f => f.Id == franchiseId).SingleOrDefaultAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return franchise; 
        }
        #endregion


        #region Update Method 
        public override void Update(params Franchise[] franchises)
        {
            try
            {
                // update the state of the franchise 
                foreach (Franchise franchise in franchises)
                {
                    _Context.Entry(franchise).State = EntityState.Modified; 
                }
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        #endregion




        #region Delete Method 
        public override void Delete(int id)
        {
            try
            {
                // delete a franchise by its id.
                Franchise franchise = _Context.Franchises.Find(id);
                _Context.Franchises.Remove(franchise);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
        #endregion



        #region Save Method 

        public async override Task<int> Save()
        {
            return await _Context.SaveChangesAsync(); 
        }
        #endregion

        public bool Exists(int id)
        {
            return _Context.Franchises.Any(f => f.Id == id);
        }


    }
}
