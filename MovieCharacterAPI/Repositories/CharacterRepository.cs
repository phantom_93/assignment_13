﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    public class CharacterRepository: Repository<ApplicationDBContext, Character>
    {

        public CharacterRepository(DbContextOptions options)
           : base(options)
        {
        }

        public IQueryable<Character> Queryable
        {
            get { return _Context.Characters.AsQueryable(); }
        }

        public override async Task<Character> Add(Character character)
        {
            try
            {
                // Add a character to the database 
                await _Context.Characters.AddAsync(character);
              
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return character;
        }


        public override async Task<IEnumerable<Character>>GetAll()
        {
            IEnumerable<Character> characters= null;
            try
            {
                // retrieve list of all the characters from the database 
                characters = await _Context.Characters.ToListAsync();
              
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return characters;
        }



        public override async Task<Character> GetById(int characterId)
        {
            Character character = null;
            try
            {
                //  get the Character by his/her id 
                character = await _Context.Characters.Where(c => c.Id == characterId).SingleOrDefaultAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return character;
        }


        public override async void Update(params Character[] characters)
        {
            try
            {
                foreach (Character character in characters)
                { 
                    // Update the state of the character 
                    _Context.Entry(character).State = EntityState.Modified;
                }

                //await Save(); 
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public override void Delete(int id)
        {

            try
            {
                // find the character by his/ her id 
                Character character = _Context.Characters.Find(id);
                _Context.Characters.Remove(character);
            }

            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async override Task<int> Save()
        {
            // save the changes to the database 
            return await _Context.SaveChangesAsync();
        }

        public bool Exists(int id)
        {
            return _Context.Characters.Any(c => c.Id == id);
        }

    }
}
