﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    // base repository inherited from IRepository
    public abstract class Repository<TContext, TType> : IRepository<TType>
    {
        public TContext _Context;

        public Repository(DbContextOptions options)
        {
            Type type = typeof(TContext);
            _Context = (TContext)Activator.CreateInstance(type, options);  
        }

        // abstract methods 
        public abstract Task<TType> Add(TType entity);
        public abstract void Delete(int id);
        public abstract Task<IEnumerable<TType>> GetAll();
        public abstract Task<TType> GetById(int id);

        public abstract void Update(params TType[] entity);
        public abstract Task<int> Save();
    }
}
