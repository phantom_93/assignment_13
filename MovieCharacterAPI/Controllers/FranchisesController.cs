﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.DTOs.Franchises;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repositories;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        // readonly variables 
        private readonly FranchiseRepository _repository;
        private readonly IMapper _mapper; 

        public FranchisesController(FranchiseRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper; 
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            IEnumerable<Franchise> franchises = await _repository.GetAll();
            IEnumerable<FranchiseDTO> franchiseDTOs = _mapper.Map<IEnumerable<FranchiseDTO>>(franchises);
            return Ok(franchiseDTOs);
        }


        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _repository.GetById(id);
            if (franchise == null)
                return NotFound();
            FranchiseDTO franchiseDTO = _mapper.Map<FranchiseDTO>(franchise);
            return franchiseDTO;
        }

        // GET: api/Franchises/Details
        [HttpGet("{id}/Details")]
        public async Task<ActionResult<FranchiseFullDTO>> GetFranchiseDetails(int id)
        {
            Franchise franchise = await _repository.Queryable
                .Where(f => f.Id.Equals(id))
                // get actors 
                .Include(f => f.Movies)
                .ThenInclude(f => f.MovieCharacters)
                .ThenInclude(f => f.Actor)

                // get characters  
                .Include(f => f.Movies)
                .ThenInclude(f => f.MovieCharacters)
                .ThenInclude(f => f.Character)


                // get movies
                .Include(f => f.Movies)
                .SingleOrDefaultAsync();


            if (franchise == null)
                return NotFound();

            FranchiseFullDTO franchisFulleDTO = _mapper.Map<FranchiseFullDTO>(franchise);
            return franchisFulleDTO;
        }

        // GET: api/Franchises/Actors
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<FranchiseActorsDTO>> GetFranchiseActors(int id)
        {
            Franchise franchise = await _repository.Queryable
                .Where(f => f.Id.Equals(id))
                .Include(f => f.Movies)
                .ThenInclude(mc => mc.MovieCharacters)
                .ThenInclude(a => a.Actor)
                .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();
            FranchiseActorsDTO franchiseActorDTO = _mapper.Map<FranchiseActorsDTO>(franchise);
            return franchiseActorDTO;
        }



        // GET: api/Franchises/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<FranchiseCharactersDTO>> GetFranchiseCharacters(int id)
        {
            Franchise franchise = await _repository.Queryable
                .Where(f=>f.Id.Equals(id))
                .Include(f=>f.Movies)
                .ThenInclude(mc=>mc.MovieCharacters)
                .ThenInclude(c => c.Character)
                 .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();
            FranchiseCharactersDTO franchiseCharactersDTO = _mapper.Map<FranchiseCharactersDTO>(franchise);
            return franchiseCharactersDTO;
        }

        // GET: api/Franchises/Movies
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<FranchiseMoviesDTO>> GetFranchiseMovies(int id)
        {
            Franchise franchise = await _repository.Queryable
              .Where(f => f.Id.Equals(id))
              .Include(f => f.Movies)
               .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();
            FranchiseMoviesDTO franchiseMoviesDTO = _mapper.Map<FranchiseMoviesDTO>(franchise);
            return franchiseMoviesDTO;
        }


        // PUT: api/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _repository.Update(franchise);

            try
            {
                await _repository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            await _repository.Add(franchise);
            await _repository.Save();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _repository.GetById(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _repository.Delete(id);
            await _repository.Save();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _repository.Exists(id);
        }
    }
}
