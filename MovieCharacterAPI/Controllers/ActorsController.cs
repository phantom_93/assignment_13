﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Contexts;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.DTOs.Actors;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repositories;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly ActorRepository _repository;
        private readonly IMapper _mapper;

        public ActorsController(ActorRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorShortDTO>>> GetActors()
        {
            IEnumerable<Actor> actors = await _repository.GetAll();

            IEnumerable<ActorShortDTO> actorShortDtos = _mapper.Map<IEnumerable<ActorShortDTO>>(actors);

            return Ok(actorShortDtos);
        }

        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorShortDTO>> GetActor(int id)
        {
            Actor actor = await _repository.GetById(id);

            if (actor == null)
            {
                return NotFound();
            }
            ActorShortDTO actorShort = _mapper.Map<ActorShortDTO>(actor);
            return actorShort;
        }
        

        // GET: api/Actors/5/All
        [HttpGet("{id}/All")]
        public async Task<ActionResult<ActorFullDTO>> GetActorDescription(int id)
        {

            Actor actor = await _repository.Queryable
                // Include Characters 
                .Include(mc => mc.MovieCharacters)
                .ThenInclude(c => c.Character)
                // Include Movie 
                .Include(mc => mc.MovieCharacters) 
                .ThenInclude(m => m.Movie)
                .Where(i=>i.Id.Equals(id))
                .SingleOrDefaultAsync();


            if (actor == null)
            {
                return NotFound();
            }
            ActorFullDTO actorfull = _mapper.Map<ActorFullDTO>(actor);
            return actorfull;
        }

        // GET: api/Actors/5/Details
        [HttpGet("{id}/Details")]
        public async Task<ActionResult<ActorDTO>> GetActorDetails(int id)
        {
            Actor actor = await _repository.GetById(id);

            if (actor == null)
            {
                return NotFound();
            }
            ActorDTO actorDTO = _mapper.Map<ActorDTO>(actor);
            return actorDTO;
        }

        // GET: api/Actors/5/Details
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<ActorCharacterDTO>> GetActorCharacters(int id)
        {
            Actor actor = await _repository.Queryable
                .Include(mc => mc.MovieCharacters)
                .ThenInclude(c => c.Character)
                .Where(i => i.Id.Equals(id))
                .SingleOrDefaultAsync();

            if (actor == null)
            {
                return NotFound();
            }
            ActorCharacterDTO actorCharacterDTO = _mapper.Map<ActorCharacterDTO>(actor);
            return actorCharacterDTO;
        }

        // GET: api/Actors/5/Details
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<ActorMovieDTO>> GetActorMovies(int id)
        {
            Actor actor = await _repository.Queryable
                .Include(mc => mc.MovieCharacters)
                .ThenInclude(m => m.Movie)
                .Where(m => m.Id.Equals(id))
                .SingleOrDefaultAsync();

            if (actor == null)
            {
                return NotFound();
            }
            ActorMovieDTO actorMovieDTO = _mapper.Map<ActorMovieDTO>(actor);
            return actorMovieDTO;
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _repository.Update(actor);

            try
            {
                await _repository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            await _repository.Add(actor);
            await _repository.Save();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
            
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            Actor actor = await _repository.GetById(id);
            if (actor == null)
            {
                return NotFound();
            }

            _repository.Delete(id);
           await _repository.Save();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _repository.Exists(id);
           
        }
    }
}
