﻿using Microsoft.CodeAnalysis.Emit;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Contexts
{
    public class ApplicationDBContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }


        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
               modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId }); // composite key 

            // seed actor Data
               modelBuilder.Entity<Actor>().HasData(
                new Actor()
                {
                    // batman, catwoman
                    Id = 1,
                    FirstName = "Christian",
                    MiddleName = "Charles Philip",
                    LastName = "Bale",
                    Gender = Gender.Male,
                    DOB = new DateTime(1974, 01, 30),
                    PlaceOfBirth = "Haverfordwest, Pembrokeshire, Wales",
                    Biography = "Bio1",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/0/0a/Christian_Bale-7837.jpg"
                }
                );

               modelBuilder.Entity<Actor>().HasData(
               new Actor()
               {
                   // superman
                   Id = 2,
                   FirstName = "Henry",
                   MiddleName = "William Dalgliesh",
                   LastName = "Cavill",
                   Gender = Gender.Male,
                   DOB = new DateTime(1983, 05, 05),
                   PlaceOfBirth = "William Dalgliesh",
                   Biography = "Bio2",
                   Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Henry_Cavill_%2848417906016%29_%28cropped%29.jpg/639px-Henry_Cavill_%2848417906016%29_%28cropped%29.jpg"
               }
               );



            modelBuilder.Entity<Actor>().HasData(
             new Actor()
             {
                 // daredevil
                 Id = 3,
                 FirstName = "Charlie",
                 MiddleName = "Thomas",
                 LastName = "Cox",
                 Gender = Gender.Male,
                 DOB = new DateTime(1982, 12, 15),
                 PlaceOfBirth = "London, England",
                 Biography = "Bio3",
                 Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Charlie_Cox_by_Gage_Skidmore.jpg/330px-Charlie_Cox_by_Gage_Skidmore.jpg"
             }
             );


              modelBuilder.Entity<Actor>().HasData(
              new Actor()
              {
                  // jessica jones 
                  Id = 4,
                  FirstName = "Krysten",
                  MiddleName = "Alyce",
                  LastName = "Ritter",
                  Gender = Gender.Female,
                  DOB = new DateTime(1981, 12, 16),
                  PlaceOfBirth = "London, England",
                  Biography = "Bio4",
                  Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Krysten_Ritter_%2836184656415%29_%28cropped%29.jpg/330px-Krysten_Ritter_%2836184656415%29_%28cropped%29.jpg"
              }
              );

            modelBuilder.Entity<Actor>().HasData(
               new Actor()
               {
                   // joker
                   Id = 5,
                   FirstName = "Joaquin",
                   MiddleName = "Rafael",
                   LastName = "Phoenix",
                   Gender = Gender.Male,
                   DOB = new DateTime(1974, 10, 28),
                   PlaceOfBirth = "San Juan, Puerto Rico",
                   Biography = "Bio6",
                   Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Joaquin_Phoenix_in_2018.jpg/330px-Joaquin_Phoenix_in_2018.jpg"
               }
               );

                  modelBuilder.Entity<Actor>().HasData(
                  new Actor()
                  {
                      // the flash
                      Id = 6,
                      FirstName = "Thomas",
                      MiddleName = "Grant",
                      LastName = "Gustin",
                      Gender = Gender.Male,
                      DOB = new DateTime(1990, 01, 14),
                      PlaceOfBirth = "Norfolk, Virginia, U.S.",
                      Biography = "Bio6",
                      Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Grant_Gustin_SDCC_2017_%28cropped%29.jpg/330px-Grant_Gustin_SDCC_2017_%28cropped%29.jpg"
                  }
                  );

                modelBuilder.Entity<Actor>().HasData(
                new Actor()
                {
                    // the punisher
                    Id = 7,
                    FirstName = "Jonathan",
                    MiddleName = "Edward",
                    LastName = "Bernthal",
                    Gender = Gender.Male,
                    DOB = new DateTime(1976, 07, 20),
                    PlaceOfBirth = "Washington, D.C., U.S.",
                    Biography = "Bio7",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Crop_of_Jon_Bernthal_by_Gage_Skidmore_3.jpg/330px-Crop_of_Jon_Bernthal_by_Gage_Skidmore_3.jpg"
                }
                );

               modelBuilder.Entity<Actor>().HasData(
               new Actor()
               {
                   // blackwidow
                   Id = 8,
                   FirstName = "Scarlett",
                   MiddleName = "Ingrid",
                   LastName = "Johansson",
                   Gender = Gender.Female,
                   DOB = new DateTime(1984, 11, 22),
                   PlaceOfBirth = "New York City, U.S.",
                   Biography = "Bio8",
                   Picture = "https://m.media-amazon.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_UY317_CR23,0,214,317_AL_.jpg"
               }
               );


              modelBuilder.Entity<Actor>().HasData(
              new Actor()
              {
                  // Mera
                  Id = 9,
                  FirstName = "Amber",
                  MiddleName = "Laura",
                  LastName = "Heard",
                  Gender = Gender.Female,
                  DOB = new DateTime(1986, 04, 22),
                  PlaceOfBirth = "Austin, Texas, U.S.",
                  Biography = "Bio9",
                  Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Amber_Heard_%2843723454772%29.jpg/330px-Amber_Heard_%2843723454772%29.jpg"
              }
              );

               modelBuilder.Entity<Actor>().HasData(
               new Actor()
               {
                   //Catwoman
                   Id = 10,
                   FirstName = "Anne",
                   MiddleName = "Jacqueline",
                   LastName = "Hathaway",
                   Gender = Gender.Female,
                   DOB = new DateTime(1982, 11, 12),
                   PlaceOfBirth = "New York City, U.S.",
                   Biography = "Bio10",
                   Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Anne_Hathaway_in_2017.png/330px-Anne_Hathaway_in_2017.png"
               }
               );

              modelBuilder.Entity<Actor>().HasData(
              new Actor()
              {
                  //reverse flash
                  Id = 11,
                  FirstName = "Matt",
                  LastName = "Letscher",
                  Gender = Gender.Male,
                  DOB = new DateTime(1970, 06, 26),
                  PlaceOfBirth = "Grosse Pointe, Michigan, U.S.",
                  Biography = "Bio11",
                  Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Matt_Letscher.jpg/330px-Matt_Letscher.jpg"
              }
              );

             modelBuilder.Entity<Actor>().HasData(
              new Actor()
              {
                      //reverse flash
                  Id = 12,
                  FirstName = "Ryan",
                  MiddleName = "Rodney",
                  LastName = "Reynolds",
                  Gender = Gender.Male,
                  DOB = new DateTime(1976, 10, 23), // October 23, 1976
                  PlaceOfBirth = "Vancouver, British Columbia, Canada",
                  Biography = "Bio12",
                  Picture = "https://media.vanityfair.com/photos/5600612b3a02e0b16051ef0e/2:3/w_640,h_960,c_limit/ryan-reynolds-best-friend-betrayal-gq-october-cover.jpg"
              }
              );



            modelBuilder.Entity<Actor>().HasData(
              new Actor()
              {
                  //supergirl
                  Id = 13,
                  FirstName = "Melissa",
                  MiddleName = "Marie",
                  LastName = "Benoist",
                  Gender = Gender.Female,
                  DOB = new DateTime(1988, 10, 04),
                  PlaceOfBirth = "Houston, Texas, U.S.",
                  Biography = "Bio12",
                  Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Melissa_Benoist_%2848477063086%29_%28cropped%29.jpg/330px-Melissa_Benoist_%2848477063086%29_%28cropped%29.jpg"
              }
              );

             modelBuilder.Entity<Actor>().HasData(
             new Actor()
             {
                 //batwoman
                 Id = 14,
                 FirstName = "Ruby",
                 MiddleName = "Rose",
                 LastName = "Langenheim",
                 Gender = Gender.Female,
                 DOB = new DateTime(1986, 03, 20),
                 PlaceOfBirth = "Houston, Texas, U.S.",
                 Biography = "Bio12",
                 Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Ruby_Rose_XXX_Return_of_Xander_Cage_premiere.png/330px-Ruby_Rose_XXX_Return_of_Xander_Cage_premiere.png"
             }
             );


            // seed franchise Data
             modelBuilder.Entity<Franchise>().HasData(
              new Franchise()
              {
                  Id = 1,
                  Name = "DC Universe",
                  Description = "The best cinematic Universe"
              }
              );
            modelBuilder.Entity<Franchise>().HasData(
             new Franchise()
             {
                 Id = 2,
                 Name = "Marvel Cinematic Universe",
                 Description = "The second to None Universe"
             }
             );

            // seed movie Data 
                   modelBuilder.Entity<Movie>().HasData(
                   new Movie()
                   {
                       // batman
                       Id = 1,
                       Title = "The Dark Knight",
                       Genre = "Action, Crime, Drama",
                       ReleaseYear = new DateTime(2008, 07, 25),  //July 25, 2008(Norway)
                       Director = "Christopher Nolan",
                       Picture = "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                       Trailer = "https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi",
                       FranchiseId = 1
                   }
                   );

                 modelBuilder.Entity<Movie>().HasData(
                  new Movie()
                  {
                      //  superman movie 
                      Id = 2,
                      Title = "Man of Steel",
                      Genre = "Action, Adventure, Sci-Fi",
                      ReleaseYear = new DateTime(2013, 06, 21), //June 21, 2013 (Norway)
                      Director = "Zack Snyder",
                      Picture = "https://m.media-amazon.com/images/M/MV5BMTk5ODk1NDkxMF5BMl5BanBnXkFtZTcwNTA5OTY0OQ@@._V1_SY1000_CR0,0,676,1000_AL_.jpg",
                      Trailer = "https://www.imdb.com/video/vi705668633?playlistId=tt0770828&ref_=tt_ov_vi",
                      FranchiseId = 1
                  }
                  );

                 modelBuilder.Entity<Movie>().HasData(
                 new Movie()
                 {
                     //  Daredevil tv series(movie) 
                     Id = 3,
                     Title = "Daredevil",
                     Genre = "Action, Crime, Drama",
                     ReleaseYear = new DateTime(2015, 04, 10), //April 10, 2015
                     Director = "Drew Goddard",
                     Picture = "https://m.media-amazon.com/images/M/MV5BODcwOTg2MDE3NF5BMl5BanBnXkFtZTgwNTUyNTY1NjM@._V1_UX182_CR0,0,182,268_AL_.jpg",
                     Trailer = "https://www.imdb.com/video/vi3349527065?playlistId=tt3322312&ref_=tt_ov_vi",
                     FranchiseId = 2
                 }
                 );

                modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    //  Jessica Jones tv series(movie) 
                    Id = 4,
                    Title = "Jessica Jones",
                    Genre = "Action, Crime, Drama",
                    ReleaseYear = new DateTime(2015, 11, 20), //November 20, 2015
                    Director = "Melissa Rosenberg",
                    Picture = "https://m.media-amazon.com/images/M/MV5BM2QyNmZkNTYtZWQyZi00NDhhLWEzMDItYmIzY2U4ZWVmOWNhXkEyXkFqcGdeQXVyNDg4NjY5OTQ@._V1_UX182_CR0,0,182,268_AL_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=nWHUjuJ8zxE",
                    FranchiseId = 2
                }
                );

             modelBuilder.Entity<Movie>().HasData(
             new Movie()
             {
                 //  Joker(movie) 
                 Id = 5,
                 Title = "Joker",
                 Genre = "Crime, Drama, Thriller",
                 ReleaseYear = new DateTime(2019, 10, 04), //October 2, 2019
                 Director = "Todd Phillips",
                 Picture = "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                 Trailer = "https://www.imdb.com/video/vi1723318041?playlistId=tt7286456&ref_=tt_ov_vi",
                 FranchiseId = 1
             }
             );


            modelBuilder.Entity<Movie>().HasData(
            new Movie()
            {
                //  the flash
                Id = 6,
                Title = "The Flash",
                Genre = "Action, Adventure, Drama",
                ReleaseYear = new DateTime(2014, 10, 07), //October 2, 2019
                Director = "Glen Winter",
                Picture = "https://m.media-amazon.com/images/M/MV5BOGU2N2M1YzgtNmE1YS00OGFhLTllMzUtYWU1MDA3NWVmYWYwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg",
                Trailer = "https://www.imdb.com/video/vi2621750809?playlistId=tt3107288&ref_=tt_ov_vi",
                FranchiseId = 1
            }
            );

            modelBuilder.Entity<Movie>().HasData(
           new Movie()
           {
               //  The punisher (movie) 
               Id = 7,
               Title = "The Punisher",
               Genre = "Action, Crime, Drama",
               ReleaseYear = new DateTime(2017, 11, 17), //November 17, 2017
               Director = "Steve Lightfoot",
               Picture = "https://m.media-amazon.com/images/M/MV5BMTExODIwOTUxNzFeQTJeQWpwZ15BbWU4MDE5MDA0MTcz._V1_UX182_CR0,0,182,268_AL_.jpg",
               Trailer = "https://www.imdb.com/video/vi1024571929?playlistId=tt5675620&ref_=tt_ov_vi",
               FranchiseId = 2
           }
           );

            modelBuilder.Entity<Movie>().HasData(
            new Movie()
            {
                //  blackwidow 
                Id = 8,
                Title = "Black Widow",
                Genre = "Action, Adventure, Sci-Fi",
                ReleaseYear = new DateTime(2020, 11, 06), //6 November 2020
                Director = "Cate Shortland",
                Picture = "https://m.media-amazon.com/images/M/MV5BMzFiODE0ZDUtN2IxNC00OTI5LTg4OWItZTE2MjU4ZTk2NjM5XkEyXkFqcGdeQXVyNDYzODU1ODM@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.imdb.com/video/vi1202175513?playlistId=tt3480822&ref_=tt_ov_vi",
                FranchiseId = 2
            }
            );


            modelBuilder.Entity<Movie>().HasData(
         new Movie()
         {
             //  Mira in Aquaman 
             Id = 9,
             Title = "Aquaman",
             Genre = "Action, Adventure, Fantasy",
             ReleaseYear = new DateTime(2018, 12, 25), // 25 December 2018
             Director = "James Wan",
             Picture = "https://m.media-amazon.com/images/M/MV5BOTk5ODg0OTU5M15BMl5BanBnXkFtZTgwMDQ3MDY3NjM@._V1_UX182_CR0,0,182,268_AL_.jpg",
             Trailer = "https://www.imdb.com/video/vi3588536857?playlistId=tt1477834&ref_=tt_ov_vi",
             FranchiseId = 1
         }
         );

    

            modelBuilder.Entity<Movie>().HasData(
          new Movie()
          {
              //  deadpool(movie)
              Id = 10,
              Title = "Deadpool",
              Genre = "Action, Adventure, Comedy",
              ReleaseYear = new DateTime(2016, 02, 12), //  October 6, 2019
              Director = "Tim Miller",
              Picture = "https://m.media-amazon.com/images/M/MV5BYzE5MjY1ZDgtMTkyNC00MTMyLThhMjAtZGI5OTE1NzFlZGJjXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg",
              Trailer = "https://www.imdb.com/video/vi567457049?playlistId=tt1431045&ref_=tt_ov_vi",
              FranchiseId = 1
          }
          );


            modelBuilder.Entity<Movie>().HasData(
             new Movie()
             {
                 // supergirl 
                 Id = 11,
                 Title = "Supergirl",
                 Genre = "Action, Adventure, Drama",
                 ReleaseYear = new DateTime(2015, 07, 08), //  July 8, 2015
                 Director = "Ali Adler, Greg Berlanti, Andrew Kreisberg",
                 Picture = "https://m.media-amazon.com/images/M/MV5BYmZmOTM3NjctZDg3Zi00NTYzLWIzY2UtYWEyOWM2NThhYzg5XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg",
                 Trailer = "https://www.imdb.com/video/vi1617148953?playlistId=tt4016454&ref_=tt_ov_vi",
                 FranchiseId = 1
             }
             );

                modelBuilder.Entity<Movie>().HasData(
               new Movie()
               {
                   //  batwoman tv series (movie)
                   Id = 12,
                   Title = "Batwoman",
                   Genre = "Action, Adventure, Crime",
                   ReleaseYear = new DateTime(2019, 10, 06), //  October 6, 2019
                   Director = "Caroline Dries",
                   Picture = "https://m.media-amazon.com/images/M/MV5BNWE4MGVhZjUtMjEwNy00NzJlLWE3ZmEtNWMyYmFlZmUzZTA0XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg",
                   Trailer = "https://www.imdb.com/video/vi1195032345?playlistId=tt8712204&ref_=tt_ov_vi",
                   FranchiseId = 1
               }
               );



            // seed Character Data 
            /*batman
            superman
            daredevil
            jessica jones
            joker
            the flash
            the punisher
            blackwidow
            Mera 
            Catwoman  
            Professor zoom/reverse flash 
            deadpool
            Supergirl
            batwoman
            */
            modelBuilder.Entity<Character>().HasData(
                new Character()
                {  //dark knight
                    Id = 1,
                    FullName = "Bruce Wayne",
                    Alias = "Dark Knight",
                    Gender = Gender.Male,
                    Picture = "https://cdn.vox-cdn.com/thumbor/2hcZHf3IO6IRSi9iyqctVhmBT8g=/1400x1050/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19736955/GettyImages_1202254631.jpg"
                }
                );

            modelBuilder.Entity<Character>().HasData(
                new Character()
                { //superman
                    Id = 2,
                    FullName = "Clark Joseph Kent",
                    Alias = "The Man of Steel",
                    Gender = Gender.Male,
                    Picture = "https://cdn.theatlantic.com/thumbor/xuePShEYRyEQec_THgWcYFhYLnw=/540x0:2340x1800/500x500/media/img/mt/2016/01/superman/original.jpg"
                }
                );

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {  //daredevil
                    Id = 3,
                    FullName = "Matthew Michael Murdock",
                    Alias = "Man Without Fear",
                    Gender = Gender.Male,
                    Picture = "https://i.pinimg.com/originals/b8/73/38/b873380b1a3f86de2cd73ebca74a6ec4.jpg"
                }
                );

            modelBuilder.Entity<Character>().HasData(
                new Character()
                { //jessica jones 
                    Id = 4,
                    FullName = "Jessica Campbell Jones Cage",
                    Alias = "Knightress",
                    Gender = Gender.Female,
                    Picture = "https://vignette.wikia.nocookie.net/marvelcinematicuniverse/images/4/4f/Jessica_Jones_Season_2_Promotional.png/revision/latest?cb=20180626040112"
                }
                );

            modelBuilder.Entity<Character>().HasData(
               new Character()
               {  // joker
                   Id = 5,
                   FullName = "Arthur Fleck",
                   Alias = "Red Hood",
                   Gender = Gender.Male,
                   Picture = "https://www.filmweb.no/incoming/article1407833.ece/REPRESENTATIONS/wide_21_1410/Joaquin%20Phoenix%20i%20Joker"
               }
               );

            modelBuilder.Entity<Character>().HasData(
               new Character()
               {  // the flash
                   Id = 6,
                   FullName = "Barry Allen",
                   Alias = "Flash",
                   Gender = Gender.Male,
                   Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNi60tY3WdHDomVGVglSrJYFJR5uArkuAxrA&usqp=CAU"
               }
               );

            modelBuilder.Entity<Character>().HasData(
              new Character()
              {  // the punisher
                  Id = 7,
                  FullName = "Frank Castle",
                  Alias = "The Punisher",
                  Gender = Gender.Male,
                  Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNi60tY3WdHDomVGVglSrJYFJR5uArkuAxrA&usqp=CAU"
              }
              );

            modelBuilder.Entity<Character>().HasData(
            new Character()
            {  // black widow
                Id = 8,
                FullName = "Natasha Romanova",
                Alias = "Black Widow",
                Gender = Gender.Female,
                Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7e0Zr64LZV6ZVHAR_qBBUOd-Y0wr2Yr98Jw&usqp=CAU"
            }
            );


            modelBuilder.Entity<Character>().HasData(
            new Character()
            {  // Mera
                Id = 9,
                FullName = "Princess of Xebel",
                Alias = "Queen Mera",
                Gender = Gender.Female,
                Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSOJbGQOUUls7xBy_Ol8NqS14iUq94Q-cvRbw&usqp=CAU"
            }
            );


            modelBuilder.Entity<Character>().HasData(
              new Character()
              {  // catwoman
                  Id = 10,
                  FullName = "Selina Kyle",
                  Alias = "The Cat",
                  Gender = Gender.Female,
                  Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTNwZSWAuL12ZIzggVy2YUAuUuuxAogiJs89g&usqp=CAU"
              }
              );


            modelBuilder.Entity<Character>().HasData(
              new Character()
              {  // Reverse Flash
                  Id = 11,
                  FullName = "Eobard Thawne",
                  Alias = "Professor Zoom",
                  Gender = Gender.Female,
                  Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2nxAhtictdvN6sS1lSP9OCsoghtlZ3ba9XQ&usqp=CAU"
              }
              );

            modelBuilder.Entity<Character>().HasData(
            new Character()
            {  //deadpool
                Id = 12,
                FullName = "Wade Winston Wilson",
                Alias = "Deadpool",
                Gender = Gender.Male,
                Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRegQXg4p5cK1YVCELa5WuV6lsqJRQdJuZ_jw&usqp=CAU"
            }
            );

            modelBuilder.Entity<Character>().HasData(
             new Character()
             {  //supergirl
                 Id = 13,
                 FullName = "Kara Zor-El",
                 Alias = "Supergirl",
                 Gender = Gender.Female,
                 Picture = "https://www.thewrap.com/wp-content/uploads/2018/09/Supergirl-1.jpg"
             }
             );

            modelBuilder.Entity<Character>().HasData(
            new Character()
            {  //batwoman
                Id = 14,
                FullName = "Kate-Kane",
                Alias = "Batwoman",
                Gender = Gender.Female,
                Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQkh6aKlkgv2qtNFg8YV0WxAmFx2hj4dqmVsA&usqp=CAU"
            }
            );

            // seed movie-character relationship 
            // batman and catwoman in the same movie 
            // the flash and reverse flash in the same movie/ serie
             modelBuilder.Entity<MovieCharacter>().HasData(
             new MovieCharacter()
             {
                 // for batman stuff
                 ActorId = 1,
                 MovieId = 1,
                 CharacterId = 1
                 
             }
              );

            modelBuilder.Entity<MovieCharacter>().HasData(
             new MovieCharacter()
             {
                 // for catwoman stuff
                 ActorId = 10,
                 MovieId = 1,
                 CharacterId = 10
                 
             }
             );

            modelBuilder.Entity<MovieCharacter>().HasData(
              new MovieCharacter()
              {
                  // for superman stuff 
                  ActorId = 2,
                  MovieId = 2,
                  CharacterId = 2

              }
              );

            modelBuilder.Entity<MovieCharacter>().HasData(
              new MovieCharacter()
              {
                  // for Daredevil stuff 
                  ActorId = 3,
                  MovieId = 3,
                  CharacterId = 3

              }
              );


            modelBuilder.Entity<MovieCharacter>().HasData(
             new MovieCharacter()
             {
                  // for jessica jones stuff 
                 ActorId = 4,
                 MovieId = 4,
                 CharacterId = 4

             }
             );

            modelBuilder.Entity<MovieCharacter>().HasData(
            new MovieCharacter()
            {
                 // for jOKER stuff 
                ActorId = 5,
                MovieId = 5,
                CharacterId = 5

            }
            );

            modelBuilder.Entity<MovieCharacter>().HasData(
            new MovieCharacter()
            {
                // for the flash stuff  
                ActorId = 6,
                MovieId = 6,
                CharacterId = 6

            }
            );


            modelBuilder.Entity<MovieCharacter>().HasData(
            new MovieCharacter()
            {
                // for the reverse flash  
                ActorId = 11,
                MovieId = 6,
                CharacterId = 11

            }
            );


             modelBuilder.Entity<MovieCharacter>().HasData(
             new MovieCharacter()
             {
                  // for the punisher 
                 ActorId = 7,
                 MovieId = 7,
                 CharacterId = 7

             }
             );


           modelBuilder.Entity<MovieCharacter>().HasData(
           new MovieCharacter()
           {
                 // for the black widow stuff   
               ActorId = 8,
               MovieId = 8,
               CharacterId = 8

           }
           );


            modelBuilder.Entity<MovieCharacter>().HasData(
            new MovieCharacter()
            {
               // for mira stuff
                ActorId = 9,
                MovieId = 9,
                CharacterId = 9

            }
            );


       
            modelBuilder.Entity<MovieCharacter>().HasData(
            new MovieCharacter()
                {
                       // for deadpool stuff
                    ActorId = 12,
                    MovieId = 10,
                    CharacterId = 12

                }
            );

            modelBuilder.Entity<MovieCharacter>().HasData(
             new MovieCharacter()
             {
                    // for batwoman stuff
                 ActorId = 14,
                 MovieId = 12,
                 CharacterId = 14

             }
             );

        }
    }
}
