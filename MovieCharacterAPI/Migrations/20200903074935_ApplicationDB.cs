﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class ApplicationDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    DOB = table.Column<DateTime>(nullable: false),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<DateTime>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(nullable: false),
                    MovieId = table.Column<int>(nullable: false),
                    ActorId = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastName", "MiddleName", "Picture", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, "Bio1", new DateTime(1974, 1, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Christian", 0, "Bale", "Charles Philip", "https://upload.wikimedia.org/wikipedia/commons/0/0a/Christian_Bale-7837.jpg", "Haverfordwest, Pembrokeshire, Wales" },
                    { 14, "Bio12", new DateTime(1986, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ruby", 1, "Langenheim", "Rose", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Ruby_Rose_XXX_Return_of_Xander_Cage_premiere.png/330px-Ruby_Rose_XXX_Return_of_Xander_Cage_premiere.png", "Houston, Texas, U.S." },
                    { 13, "Bio12", new DateTime(1988, 10, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Melissa", 1, "Benoist", "Marie", "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Melissa_Benoist_%2848477063086%29_%28cropped%29.jpg/330px-Melissa_Benoist_%2848477063086%29_%28cropped%29.jpg", "Houston, Texas, U.S." },
                    { 12, "Bio12", new DateTime(1976, 10, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ryan", 0, "Reynolds", "Rodney", "https://media.vanityfair.com/photos/5600612b3a02e0b16051ef0e/2:3/w_640,h_960,c_limit/ryan-reynolds-best-friend-betrayal-gq-october-cover.jpg", "Vancouver, British Columbia, Canada" },
                    { 10, "Bio10", new DateTime(1982, 11, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Anne", 1, "Hathaway", "Jacqueline", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Anne_Hathaway_in_2017.png/330px-Anne_Hathaway_in_2017.png", "New York City, U.S." },
                    { 9, "Bio9", new DateTime(1986, 4, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Amber", 1, "Heard", "Laura", "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Amber_Heard_%2843723454772%29.jpg/330px-Amber_Heard_%2843723454772%29.jpg", "Austin, Texas, U.S." },
                    { 8, "Bio8", new DateTime(1984, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Scarlett", 1, "Johansson", "Ingrid", "https://m.media-amazon.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_UY317_CR23,0,214,317_AL_.jpg", "New York City, U.S." },
                    { 11, "Bio11", new DateTime(1970, 6, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Matt", 0, "Letscher", null, "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Matt_Letscher.jpg/330px-Matt_Letscher.jpg", "Grosse Pointe, Michigan, U.S." },
                    { 6, "Bio6", new DateTime(1990, 1, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Thomas", 0, "Gustin", "Grant", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Grant_Gustin_SDCC_2017_%28cropped%29.jpg/330px-Grant_Gustin_SDCC_2017_%28cropped%29.jpg", "Norfolk, Virginia, U.S." },
                    { 5, "Bio6", new DateTime(1974, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Joaquin", 0, "Phoenix", "Rafael", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Joaquin_Phoenix_in_2018.jpg/330px-Joaquin_Phoenix_in_2018.jpg", "San Juan, Puerto Rico" },
                    { 4, "Bio4", new DateTime(1981, 12, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Krysten", 1, "Ritter", "Alyce", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Krysten_Ritter_%2836184656415%29_%28cropped%29.jpg/330px-Krysten_Ritter_%2836184656415%29_%28cropped%29.jpg", "London, England" },
                    { 3, "Bio3", new DateTime(1982, 12, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Charlie", 0, "Cox", "Thomas", "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Charlie_Cox_by_Gage_Skidmore.jpg/330px-Charlie_Cox_by_Gage_Skidmore.jpg", "London, England" },
                    { 2, "Bio2", new DateTime(1983, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Henry", 0, "Cavill", "William Dalgliesh", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Henry_Cavill_%2848417906016%29_%28cropped%29.jpg/639px-Henry_Cavill_%2848417906016%29_%28cropped%29.jpg", "William Dalgliesh" },
                    { 7, "Bio7", new DateTime(1976, 7, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jonathan", 0, "Bernthal", "Edward", "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Crop_of_Jon_Bernthal_by_Gage_Skidmore_3.jpg/330px-Crop_of_Jon_Bernthal_by_Gage_Skidmore_3.jpg", "Washington, D.C., U.S." }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 9, "Queen Mera", "Princess of Xebel", 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSOJbGQOUUls7xBy_Ol8NqS14iUq94Q-cvRbw&usqp=CAU" },
                    { 14, "Batwoman", "Kate-Kane", 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQkh6aKlkgv2qtNFg8YV0WxAmFx2hj4dqmVsA&usqp=CAU" },
                    { 13, "Supergirl", "Kara Zor-El", 1, "https://www.thewrap.com/wp-content/uploads/2018/09/Supergirl-1.jpg" },
                    { 12, "Deadpool", "Wade Winston Wilson", 0, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRegQXg4p5cK1YVCELa5WuV6lsqJRQdJuZ_jw&usqp=CAU" },
                    { 11, "Professor Zoom", "Eobard Thawne", 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2nxAhtictdvN6sS1lSP9OCsoghtlZ3ba9XQ&usqp=CAU" },
                    { 10, "The Cat", "Selina Kyle", 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTNwZSWAuL12ZIzggVy2YUAuUuuxAogiJs89g&usqp=CAU" },
                    { 8, "Black Widow", "Natasha Romanova", 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7e0Zr64LZV6ZVHAR_qBBUOd-Y0wr2Yr98Jw&usqp=CAU" },
                    { 1, "Dark Knight", "Bruce Wayne", 0, "https://cdn.vox-cdn.com/thumbor/2hcZHf3IO6IRSi9iyqctVhmBT8g=/1400x1050/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19736955/GettyImages_1202254631.jpg" },
                    { 6, "Flash", "Barry Allen", 0, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNi60tY3WdHDomVGVglSrJYFJR5uArkuAxrA&usqp=CAU" },
                    { 5, "Red Hood", "Arthur Fleck", 0, "https://www.filmweb.no/incoming/article1407833.ece/REPRESENTATIONS/wide_21_1410/Joaquin%20Phoenix%20i%20Joker" },
                    { 4, "Knightress", "Jessica Campbell Jones Cage", 1, "https://vignette.wikia.nocookie.net/marvelcinematicuniverse/images/4/4f/Jessica_Jones_Season_2_Promotional.png/revision/latest?cb=20180626040112" },
                    { 3, "Man Without Fear", "Matthew Michael Murdock", 0, "https://i.pinimg.com/originals/b8/73/38/b873380b1a3f86de2cd73ebca74a6ec4.jpg" },
                    { 2, "The Man of Steel", "Clark Joseph Kent", 0, "https://cdn.theatlantic.com/thumbor/xuePShEYRyEQec_THgWcYFhYLnw=/540x0:2340x1800/500x500/media/img/mt/2016/01/superman/original.jpg" },
                    { 7, "The Punisher", "Frank Castle", 0, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNi60tY3WdHDomVGVglSrJYFJR5uArkuAxrA&usqp=CAU" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The best cinematic Universe", "DC Universe" },
                    { 2, "The second to None Universe", "Marvel Cinematic Universe" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Christopher Nolan", 1, "Action, Crime, Drama", "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2008, 7, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Dark Knight", "https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi" },
                    { 2, "Zack Snyder", 1, "Action, Adventure, Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMTk5ODk1NDkxMF5BMl5BanBnXkFtZTcwNTA5OTY0OQ@@._V1_SY1000_CR0,0,676,1000_AL_.jpg", new DateTime(2013, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Man of Steel", "https://www.imdb.com/video/vi705668633?playlistId=tt0770828&ref_=tt_ov_vi" },
                    { 5, "Todd Phillips", 1, "Crime, Drama, Thriller", "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2019, 10, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Joker", "https://www.imdb.com/video/vi1723318041?playlistId=tt7286456&ref_=tt_ov_vi" },
                    { 6, "Glen Winter", 1, "Action, Adventure, Drama", "https://m.media-amazon.com/images/M/MV5BOGU2N2M1YzgtNmE1YS00OGFhLTllMzUtYWU1MDA3NWVmYWYwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg", new DateTime(2014, 10, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Flash", "https://www.imdb.com/video/vi2621750809?playlistId=tt3107288&ref_=tt_ov_vi" },
                    { 9, "James Wan", 1, "Action, Adventure, Fantasy", "https://m.media-amazon.com/images/M/MV5BOTk5ODg0OTU5M15BMl5BanBnXkFtZTgwMDQ3MDY3NjM@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2018, 12, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aquaman", "https://www.imdb.com/video/vi3588536857?playlistId=tt1477834&ref_=tt_ov_vi" },
                    { 10, "Tim Miller", 1, "Action, Adventure, Comedy", "https://m.media-amazon.com/images/M/MV5BYzE5MjY1ZDgtMTkyNC00MTMyLThhMjAtZGI5OTE1NzFlZGJjXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2016, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Deadpool", "https://www.imdb.com/video/vi567457049?playlistId=tt1431045&ref_=tt_ov_vi" },
                    { 11, "Ali Adler, Greg Berlanti, Andrew Kreisberg", 1, "Action, Adventure, Drama", "https://m.media-amazon.com/images/M/MV5BYmZmOTM3NjctZDg3Zi00NTYzLWIzY2UtYWEyOWM2NThhYzg5XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg", new DateTime(2015, 7, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supergirl", "https://www.imdb.com/video/vi1617148953?playlistId=tt4016454&ref_=tt_ov_vi" },
                    { 12, "Caroline Dries", 1, "Action, Adventure, Crime", "https://m.media-amazon.com/images/M/MV5BNWE4MGVhZjUtMjEwNy00NzJlLWE3ZmEtNWMyYmFlZmUzZTA0XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY268_CR16,0,182,268_AL_.jpg", new DateTime(2019, 10, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Batwoman", "https://www.imdb.com/video/vi1195032345?playlistId=tt8712204&ref_=tt_ov_vi" },
                    { 3, "Drew Goddard", 2, "Action, Crime, Drama", "https://m.media-amazon.com/images/M/MV5BODcwOTg2MDE3NF5BMl5BanBnXkFtZTgwNTUyNTY1NjM@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2015, 4, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Daredevil", "https://www.imdb.com/video/vi3349527065?playlistId=tt3322312&ref_=tt_ov_vi" },
                    { 4, "Melissa Rosenberg", 2, "Action, Crime, Drama", "https://m.media-amazon.com/images/M/MV5BM2QyNmZkNTYtZWQyZi00NDhhLWEzMDItYmIzY2U4ZWVmOWNhXkEyXkFqcGdeQXVyNDg4NjY5OTQ@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2015, 11, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jessica Jones", "https://www.youtube.com/watch?v=nWHUjuJ8zxE" },
                    { 7, "Steve Lightfoot", 2, "Action, Crime, Drama", "https://m.media-amazon.com/images/M/MV5BMTExODIwOTUxNzFeQTJeQWpwZ15BbWU4MDE5MDA0MTcz._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2017, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Punisher", "https://www.imdb.com/video/vi1024571929?playlistId=tt5675620&ref_=tt_ov_vi" },
                    { 8, "Cate Shortland", 2, "Action, Adventure, Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMzFiODE0ZDUtN2IxNC00OTI5LTg4OWItZTE2MjU4ZTk2NjM5XkEyXkFqcGdeQXVyNDYzODU1ODM@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2020, 11, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Black Widow", "https://www.imdb.com/video/vi1202175513?playlistId=tt3480822&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieId", "CharacterId", "ActorId", "Picture" },
                values: new object[,]
                {
                    { 1, 1, 1, null },
                    { 1, 10, 10, null },
                    { 2, 2, 2, null },
                    { 5, 5, 5, null },
                    { 6, 6, 6, null },
                    { 6, 11, 11, null },
                    { 9, 9, 9, null },
                    { 10, 12, 12, null },
                    { 12, 14, 14, null },
                    { 3, 3, 3, null },
                    { 4, 4, 4, null },
                    { 7, 7, 7, null },
                    { 8, 8, 8, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_ActorId",
                table: "MovieCharacters",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
