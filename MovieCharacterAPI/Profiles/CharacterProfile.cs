﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Characters;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieCharacterAPI.Profiles
{
    public class CharacterProfile: Profile
    {
        public CharacterProfile()
        {
            // Map the 'Character' model to the CharacterDTO Data Transfer Object 
            CreateMap<Character, CharacterDTO>();

            // Map the 'Character' model to the CharacterShortDTO Data Transfer Object 
            CreateMap<Character, CharacterShortDTO>();
        }
    }
}
