﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.DTOs.Actors;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.ProfilesProfile
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            // Map the 'Actor' model to the ActorShortDTO Data Transfer Object
            CreateMap<Actor, ActorShortDTO>();

            // Map the 'Actor' model to the ActorDTO Data Transfer Object
            CreateMap<Actor, ActorDTO>();

            // Map the 'Actor' model to the ActorCharacterDTO Data Transfer Object 
            /*retrieve characters from moviecharacters*/
            CreateMap<Actor, ActorCharacterDTO>()
                 .ForMember(c => c.Characters, opt => opt
                    .MapFrom(mc => mc.MovieCharacters
                        .Select(c => c.Character)
                        .Distinct().ToList()));


            // Map the 'Actor' model to the ActorMovieDTO Data Transfer Object 
            /*retrieve movies from moviecharacters*/
            CreateMap<Actor, ActorMovieDTO>()
                .ForMember(c => c.Movies, opt => opt
                    .MapFrom(mc => mc.MovieCharacters
                        .Select(c => c.Movie)
                        .Distinct().ToList()));

            // Map the 'Actor' model to the ActorFullrDTO Data Transfer Object 
            /*Retrieve the movies and characters from joint tables */
            CreateMap<Actor, ActorFullDTO>()
                .ForMember(c => c.Characters, opt => opt
                    .MapFrom(mc => mc.MovieCharacters
                        .Select(c => c.Character)
                        .Distinct().ToList()))
                .ForMember(c => c.Movies, opt => opt
                    .MapFrom(mc => mc.MovieCharacters
                        .Select(c => c.Movie)
                        .Distinct().ToList()));
        }
    }
}
