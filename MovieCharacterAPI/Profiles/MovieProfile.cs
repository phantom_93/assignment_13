﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Movies;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Map the 'Movie' model to the MovieDTO Data Transfer Object
            CreateMap<Movie, MovieDTO>();

            // Map the 'Movie' model to the MovieDTO Data Transfer Object
            CreateMap<Movie, MovieShortDTO>();

            // Map the 'Movie' model to the MovieActorDTO Data Tansfer Object 
            // retrieve actors through MovieCharacters
            CreateMap<Movie, MovieActorDTO>()
                .ForMember(a=> a.Actors, opt => opt
                .MapFrom(mc => mc.MovieCharacters
                        .Select(m => m.Actor)
                        .Distinct().ToList()));


            //Map the 'Movie' model to the MovieCharacterDTO Data Tansfer Object 
            // retrieve characters through MovieCharacters
            CreateMap<Movie, MovieCharacterDTO>()
                 .ForMember(a => a.Characters, opt => opt
                 .MapFrom(mc => mc.MovieCharacters
                         .Select(m => m.Character)
                         .Distinct().ToList()));


        }
    }
}
 