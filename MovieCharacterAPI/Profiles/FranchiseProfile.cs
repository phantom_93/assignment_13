﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Franchises;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile: Profile
    {
        public FranchiseProfile()
        {
            // Map the 'Franchise' model to the FranchiseDTO Data Transfer Object
            CreateMap<Franchise, FranchiseDTO>();

            // Map the 'Franchise' model to the FranchiseDTO Data Transfer Object
            /*retrieve actors through movieCharaters*/
            CreateMap<Franchise, FranchiseActorsDTO>()
            .ForMember(fa => fa.Actors, opt => opt
                    .MapFrom(mc => mc.Movies
                        .SelectMany(mc => mc.MovieCharacters)
                        .Select(m => m.Actor)
                        .Distinct().ToList()));

            // Map the 'Franchise' model to the FranchiseCharacterDTO Data Transfer Object
            /*retrieve characters through movieCharaters*/
            CreateMap<Franchise, FranchiseCharactersDTO>()
                .ForMember(c => c.Characters, opt => opt
                .MapFrom(mc => mc.Movies
                 .SelectMany(mc => mc.MovieCharacters)
                        .Select(m => m.Character)
                        .Distinct().ToList()));

            // Map the 'Franchise' model to the FranchiseMovieDTO Data Transfer Object
            /*retrieve movies through movieCharaters*/
            CreateMap<Franchise, FranchiseMoviesDTO>()
             .ForMember(fc => fc.Movies, opt => opt
                .MapFrom(mc => mc.Movies
                 .SelectMany(mc => mc.MovieCharacters)
                        .Select(fm => fm.Character)
                        .Distinct().ToList()));



            // Map the 'Franchise' model to the FranchiseFullDTO Data Transfer Object
            /*retrieve actors, characters and movies through MovieCharaters*/

            CreateMap<Franchise, FranchiseFullDTO>()
                 //map actors 
                 .ForMember(fa => fa.Actors, opt => opt
                    .MapFrom(mc => mc.Movies
                        .SelectMany(mc => mc.MovieCharacters)
                        .Select(m => m.Actor)
                        .Distinct().ToList()))

                // map characters 
            
                .ForMember(c => c.Characters, opt => opt
                .MapFrom(mc => mc.Movies
                 .SelectMany(mc => mc.MovieCharacters)
                        .Select(m => m.Character)
                        .Distinct().ToList()))

             // map movies 
             .ForMember(fc => fc.Movies, opt => opt
                .MapFrom(mc => mc.Movies
                 .SelectMany(mc => mc.MovieCharacters)
                        .Select(fm => fm.Character)
                        .Distinct().ToList()));


        }
    }
}
