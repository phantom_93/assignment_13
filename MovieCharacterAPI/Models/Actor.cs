﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Models
{
    public enum Gender
    {
        Male, 
        Female
    }
    public class Actor
    {
       
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }


        public string LastName { get; set; }

        public Gender Gender { get; set; }
        public DateTime DOB { get; set; } // Date of birth

        public string PlaceOfBirth { get; set; }

        public string Biography { get; set; }

        public string Picture { get; set; }

        public IEnumerable<MovieCharacter> MovieCharacters { get; set; }

        
    }
}
