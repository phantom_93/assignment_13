﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Models
{
    public class Movie
    {
       public int Id { get; set; } // Primary key

       public string Title { get; set; }

       public string Genre { get; set; }

       public DateTime ReleaseYear { get; set; }

       public string Director { get; set; }

       public string Picture { get; set; } // url ink 
       
        public IEnumerable<MovieCharacter> MovieCharacters { get; set;}


        public string Trailer { get; set; } // youtube or imdb link

        // one-to-many relationship between one franchise and many movies 
        public int? FranchiseId { get; set; }

        public Franchise Franchise { get; set; }
    }
}
