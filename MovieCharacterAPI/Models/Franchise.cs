﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Models
{
    public class Franchise
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

       

        public IEnumerable<Movie> Movies { get; set; }
        public Franchise()
        {
            Movies = new List<Movie>();
        }
    }
}
